<?php
namespace Admin\Controller;
use Think\Controller;
use Common\Controller\AdminController;
class IndexController extends AdminController {
    public function index(){
        $this->display();
    }
    public function discount(){
        $this->display();
    }
    public function food(){
        $this->display();
    }
    public function login(){
        $this->display();
    }
    public function member(){
        $this->display();
    }
    public function movie(){
        $this->display();
    }
    public function session(){
        $this->display();
    }
    public function theater(){
        $this->display();
    }
    public function ticket(){
        $this->display();
    }
    public function ticketprice(){
        $this->display();
    }
}